#!/usr/bin/env python3
import os
import subprocess
import shutil

if __name__ == '__main__':
    if not os.path.exists("repos"):
        shutil.copy("repos.base", "repos")

    if not os.path.exists("conanws.yml"):
        shutil.copy("conanws.base.yml", "conanws.yml")

    repo_paths = [line.strip() for line in open("repos").readlines()]

    workspace_url = subprocess.getoutput("git remote get-url origin")
    for dir_ in repo_paths:
        remote_url = os.path.dirname(workspace_url) + f"/{dir_}.git"
        subprocess.check_call(f"git clone {remote_url}".split(" "))
